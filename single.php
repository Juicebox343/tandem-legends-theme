<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Tandem_Legends
 */

get_header();
?>
<div id='container'>
	<main id="primary" class="site-main">
		<?php
		while ( have_posts() ) :
			?>
			
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				<div class="entry-meta">
				<?php	tandem_legends_posted_on();	?>
			</div><!-- .entry-meta -->
			</header><!-- .entry-header -->
			
			<?php
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			the_post_navigation(
				array(
					'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous', 'tandem-legends' ),
					'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next', 'tandem-legends' ),
				)
			);

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

	</main><!-- #main -->

<?php
get_sidebar();
?>
</div>

<?php
get_footer();
