<?php
/*
    Template Name: Home Page
*/

$podcast_parent = 397;
$episode_ids = get_category($podcast_parent);


class CategoryObject {
    public $image;
    public $category_id;
    public $count;
    public $friendly_name;

    function __construct($category_id, $slug, $friendly_name, $count, $image) {
        $this->category_id = $category_id;
        $this->slug = $slug;
        $this->friendly_name = $friendly_name;
        $this->count = $count;
        $this->image = $image;
      }

    function get_image() {
        return $this->image;
    }

    function get_category_id() {
        return $this->category_id;
    }
}


$args = array( 'child_of' => $podcast_parent, );
$categories = get_categories( $args );
$category_array = array();
foreach($categories as $category) { 
    $id = $category->term_id;
    $slug = $category->slug;
    $name = get_field('friendly_name', 'category_' . $id);
    $count = get_category($id)->count;
    $image = get_field('category_featured_image', 'category_' . $id);
    $category_array[] = new CategoryObject($id, $slug, $name, $count, $image);
}



get_header();
?>


<main>
        <section id='hero-section'>
            <div class='container'>

                <?php
                global $post;
                $recent_posts = get_posts(array(
                    'numberposts' => 1, // Number of recent posts thumbnails to display
                    'category' => 4 // Show only the published posts
                ));
                
                foreach($recent_posts as $post){
                    setup_postdata($post); 
            
                    get_template_part( 'template-parts/content-hero-episode', get_post_type() );
    
                }
                wp_reset_postdata(); // important
                wp_reset_query(); ?>
            </div>
        </section>
        
        <section id='overview-section'>
            <div class='container'>
                <p>Sit down with your hosts, Shannon and Joe, as they play, discuss and theorize their way through Nintendo’s Legend of Zelda video games in the timeline order noted in the Hyrule Historia.</p> 
            </div>
        </section>

        <section id='timeline-section'>
        <div class='container'>
            <h2 class="section-title">Follow along in timeline order!</h2>
            <ol id='timeline'>
                <li id='pre-timeline' class='timeline-group'>
                    <h3>Pre-History</h3>
                    <ol class='timeline-entries'>
                        <li><a href='https://tandemlegends.com/category/gameplay/the-legend-of-zelda-skyward-sword/' ><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/The_Legend_of_Zelda_-_Skyward_Sword_(logo).png'><span class='slide-over' alt='Skyward Sword Logo'>Skyward Sword</span></a></a></li>
                        <li><a href='https://tandemlegends.com/category/gameplay/the-legend-of-zelda-the-minish-cap/'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/The_Legend_of_Zelda_-_The_Minish_Cap_(logo).png'><span class='slide-over' alt='The Minish Cap Logo'>Minish Cap</span></a></li>
                        <li><a href='https://tandemlegends.com/category/gameplay/the-legend-of-zelda-four-swords/'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/The_Legend_of_Zelda_-_Four_Swords_(logo).png'><span class='slide-over'alt='Four Swords Logo'>Four Swords</span></a></li>
                        <li><a href='https://tandemlegends.com/category/gameplay/the-legend-of-zelda-ocarina-of-time/'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/The_Legend_of_Zelda_-_Ocarina_of_Time_(logo).png'><span class='slide-over' alt='Ocarina of Time Logo'>Ocarina of Time</span></a></li>
                    </ol>
                </li>
                <li id='child-timeline' class='timeline-group'>
                    <h3>Child Timeline</h3>
                    <ol class='timeline-entries'>
                        <li><a href='https://tandemlegends.com/category/gameplay/the-legend-of-zelda-majoras-mask/'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/The_Legend_of_Zelda_-_Majoras_Mask_(logo).png'><span class='slide-over' alt="Majora's Mask Logo">Majora's Mask</span></span></a></li>
                        <li class='playing-now'><a href='https://tandemlegends.com/category/gameplay/the-legend-of-zelda-twilight-princess/'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/The_Legend_of_Zelda_-_Twilight_Princess_(logo).png' alt='Twilight Princess Logo'><span class='slide-over'>Twilight Princess</span></a></li>
                        <li class='not-yet'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/The_Legend_of_Zelda_-_Four_Swords_Adventures_(logo).png'><span class='slide-over' alt='Four Swords Adventures Logo'>Four Swords Adventures</span></a></li>
                    </ol>
                </li>
                <li id='downfall-timeline' class='timeline-group'>
                    <h3>Downfall</h3>
                    <ol class='timeline-entries'>
                        <li class='not-yet'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/The_Legend_of_Zelda_-_A_Link_to_the_Past_(logo).png' alt='A Link to the Past Logo'><span class='slide-over'>A Link to the Past</span></li>
                        <li class='not-yet'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/Oracle_of_Ages_Logo.png' alt='Oracle of Ages Logo'><span class='slide-over'>Oracle of Ages</span></li>
                        <li class='not-yet'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/Oracle_of_Seasons_Logo.png' alt='Oracle of Seasons Logo'><span class='slide-over'>Oracle of Seasons</span></li>
                        <li class='not-yet'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/The_Legend_of_Zelda_-_Links_Awakening_(logo).png' alt="Link's Awakening Logo"><span class='slide-over'>Link's Awakening</span></li>
                        <li class='not-yet'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/The_Legend_of_Zelda_-_A_Link_Between_Worlds_(logo).png' alt='A Link Between Worlds Logo'><span class='slide-over'>A Link Between Worlds</span></li>
                        <li class='not-yet'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/Tri_Force_Heroes_logo.png' alt='Triforce Heroes Logo'><span class='slide-over'>Tri Force Heroes</span></li>
                        <li class='not-yet'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/The_Legend_of_Zelda_(logo).png' alt='The Legend of Zelda Logo'><span class='slide-over'>The Legend of Zelda</span></li>
                        <li class='not-yet'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/Zelda_II_-_The_Adventure_of_Link_(logo).png' alt='Zelda 2 - The Adventure of Link Logo'><span class='slide-over'>The Adventure of Link</span></li>
                    </ol>
                </li>
                <li id='adult-timeline' class='timeline-group'>
                    <h3>Adult Timeline</h3>
                    <ol class='timeline-entries'>
                        <li class='not-yet'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/The_Legend_of_Zelda_-_The_Wind_Waker_(logo).png' alt='The Wind Waker Logo'><span class='slide-over'>The Wind Waker</span></li>
                        <li class='not-yet'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/The_Legend_of_Zelda_-_Phantom_Hourglass_(logo).png' alt='Phantom Hourglass Logo'><span class='slide-over'>Phantom Hourglass</span></li>
                        <li class='not-yet'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/The_Legend_of_Zelda_-_Spirit_Tracks_(logo).png' alt='Spirit Tracks Logo'><span class='slide-over'>Spirit Tracks</span></li>
                    </ol>
                </li>
                <li id='new-timeline' class='timeline-group'>
                    <h3>Far Future</h3>
                    <ol class='timeline-entries'>
                        <li class='not-yet'><img class='no-image' src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/Hyrule_Warriors_Logo.png' alt='Blurred Out Logo'><span class='slide-over'>Hyrule Warriors Age of Calamity</span></li>
                        <li class='not-yet'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/BotW_NA_Logo_White.png' alt='Breath of the Wild Logo'><span class='slide-over'>Breath of the Wild</span></li>
                        <li class='not-yet'><img class='no-image' src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/BotW_NA_Logo_White.png' alt='Blurred Out Logo'><span class='slide-over'>Breath of the Wild 2</span></li>
                    </ol> 
                </li>
                <li id='alt-timeline' class='timeline-group'>
                    <h3>Alternate Worlds</h3>
                    <ol class='timeline-entries'>
                        <li><a href='https://www.tandemlegends.com/category/sidequests/animated-series/'><img id='lozTV' src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/loz_tv.png'><span class='slide-over'>Animated Series</span></a></li>
                        <li class='not-yet'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/games/Hyrule_Warriors_Logo.png'><span class='slide-over'>Hyrule Warriors</span></li>
                    </ol> 
                </li>
            </ol>
        </div>
    </section>



    <section id='filler'>
        <div class='container'>
            <p>New episodes of Tandem Legends: a Legend of Zelda Podcast released every other week.</p>
            <p class='shiny-text'>More or less.</p>
        </div>
    </section>



</main>


<?php
get_footer();
?>