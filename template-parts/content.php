<?php
/**
 * Template part for displaying posts
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 * @package Tandem_Legends
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php
		if ( ! is_singular() || is_front_page() ) : ?> 
			<header class="entry-header">
			<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
			<div class="entry-meta">
			<?php tandem_legends_posted_on(); ?>
		</div><!-- .entry-meta -->
		</header><!-- .entry-header -->
		<?php	endif; ?>


	<?php if ( ! is_singular() || is_front_page() ) : 
			tandem_legends_post_thumbnail(); 
	endif; ?>

	<div class="entry-content">
		<div>
		<?php if ( ! is_singular() || is_front_page() ) : ?>
			<?php the_excerpt(); echo '<a class="readMore" href="' . esc_url( get_permalink() ) . '"> [Read More]</a>' ?>
			
		<?php
		else: 
		?>
		<div class='post-body'>
			<?php the_content(); ?>
			<div class='tags'>
				<?php the_tags('<h3>Discussed in this episode:</h3> <div class="all-tags">', ', ' ,'</div>');?>
				<hr>
			</div>
		</div>
	<?php
		endif;
	?>
		</div>

	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php tandem_legends_entry_footer(); ?>
	</footer><!-- .entry-footer -->

</article><!-- #post-<?php the_ID(); ?> -->
