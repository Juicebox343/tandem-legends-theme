<?php
/**
 * Template part for displaying posts
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 * @package Tandem_Legends
 */
?>

<article id="post-<?php the_ID(); ?>"  <?php post_class(); ?> >
	<header class="entry-header">
		<?php
		if ( ! is_singular() || is_front_page() ) :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
				<?php
				tandem_legends_posted_on();
				?>
			</div><!-- .entry-meta -->

		<?php endif; ?>
	</header><!-- .entry-header -->


		<?php tandem_legends_post_thumbnail(); ?>


	<div class="entry-content">
			<?php the_excerpt(); ?>
			<?php echo do_shortcode('[spp-player]'); ?>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->





