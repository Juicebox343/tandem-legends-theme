<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Tandem_Legends
 */

$podcast_parent = 397;
$episode_ids = get_category($podcast_parent);

class CategoryObject {
    public $image;
    public $category_id;
    public $count;
    public $friendly_name;

    function __construct($category_id, $slug, $friendly_name, $count, $image) {
        $this->category_id = $category_id;
        $this->slug = $slug;
        $this->friendly_name = $friendly_name;
        $this->count = $count;
        $this->image = $image;
      }

    function get_image() {
        return $this->image;
    }

    function get_category_id() {
        return $this->category_id;
    }
}

$args = array( 'child_of' => $podcast_parent, );
$categories = get_categories( $args );
$category_array = array();
foreach($categories as $category) { 
    $id = $category->term_id;
    $slug = $category->slug;
    $name = get_field('friendly_name', 'category_' . $id);
    $count = get_category($id)->count;
    $image = get_field('category_featured_image', 'category_' . $id);
    $category_array[] = new CategoryObject($id, $slug, $name, $count, $image);
}

 
get_header();
?>

	<main id="primary" class="site-main">

		<section class="error-404 not-found">
			<header class="page-header">
				<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'tandem-legends' ); ?></h1>
			</header><!-- .page-header -->

			<div class="page-content">
				<img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/error_transparent.png'>'
			
					<div class="widget widget_categories">
						<h2 class="widget-title"><?php esc_html_e( 'Looking for any of these?', 'tandem-legends' ); ?></h2>
						<ul class="carousel">
						<?php foreach($category_array as $category) { ?>
                            <li>
                                <a href='/category/gameplay/<?php echo $category->slug; ?>/'>
                                    <img class='category-image' src='<?php echo $category->image['url'];?>' alt='<?php echo $category->image['alt']?>'>
                                    <span class='number-of-episodes'><?php echo $category->friendly_name;?><br>Episodes: <?php echo $category->count;?></span>
                                </a>
                            </a></li>
                        <?php } ?>
						</ul>
					</div><!-- .widget -->

					<?php
					the_widget( 'WP_Widget_Tag_Cloud' );
					?>

			</div><!-- .page-content -->
		</section><!-- .error-404 -->

	</main><!-- #main -->

<?php
get_footer();

?>