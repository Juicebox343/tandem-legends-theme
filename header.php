<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Tandem_Legends
 */

 //Adv Custom Fields

$overlay_image_1    = get_field('entry_image_1', 1648);
$overlay_link_1    	= get_field('entry_link_1', 1648);
$overlay_image_2    = get_field('entry_image_2', 1648);
$overlay_link_2    	= get_field('entry_link_2', 1648);
$overlay_image_3    = get_field('entry_image_3', 1648);
$overlay_link_3    	= get_field('entry_link_3', 1648);

?>




<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">



	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Alata&family=Raleway&family=Sanchez&display=swap" rel="stylesheet"> 

	<?php wp_head(); ?>

	<!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'tandem-legends' ); ?></a>

	<header>
		<nav id="site-navigation" class="main-navigation">
			<!-- <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
			<div class='bar-container'>
				<div class="bar1"></div>
				<div class="bar2"></div>
				<div class="bar3"></div>
				<div class="bar4"></div>
				<div class="bar5"></div>
				<div class="bar6"></div>
			</div>
				
			</button> -->
		<?php
				wp_nav_menu( array(
					'theme_location'	=> 'main-menu',
					'menu_id'			=> 'primary-menu',

				));
			?>	

			<?php if ( is_active_sidebar( 'navbar-1' ) ) : ?> 
					<?php dynamic_sidebar( 'navbar-1' ); ?>
			<?php endif; ?>
			</div> 
			<div class='social-menu'>
				<ul>
					<li><a href="mailto:tandemlegends@gmail.com"><i class="fas fa-envelope"></i></a></li>
					<li><a href="https://twitter.com/TandemLegends"><i class="fab fa-twitter"></i></a></li>
					<li class='kofi-menu'><a href="https://ko-fi.com/tandemlegendspod"><img alt='Buy us a coffee on Ko-fi.com' src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/kofi.png"></a></li>
				</ul>
			</div>
			
				
			</div>
		</nav>

		<section class='banner'>
			<div class='logo-container'>
				<img class='logo' alt='Tandem Legends - A Legend of Zelda Podcast' src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/tandem_logo_transparent.png">
				<img class='logo-small' alt='Tandem Legends - A Legend of Zelda Podcast' src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/Tandem Title.png">
			</div>
				<ul class='subscribe-here-container'>
					<li class='subscribe-here-platform'>
						<a href='https://podcasts.google.com/feed/aHR0cHM6Ly90YW5kZW1sZWdlbmRzLmxpYnN5bi5jb20vcnNz?sa=X&ved=2ahUKEwiRhI7e7IzsAhUJzp4KHS58CoEQ9sEGegQIARAC'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/google_podcasts_banner.png' alt='Listen on Google Podcasts'></a>
					</li>
					<li class='subscribe-here-platform'>
						<a href='https://www.stitcher.com/show/tandem-legends'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/stitcher_banner.png' alt='Listen on Stitcher'></a>
					</li>
					<li class='subscribe-here-platform'>
						<a href='https://podcasts.apple.com/us/podcast/tandem-legends-a-legend-of-zelda-podcast/id1251557201'><img src='<?php bloginfo('stylesheet_directory'); ?>/assets/images/apple_podcasts_banner.png' alt='Listen on Apple Podcasts'></a>
					</li>
				</ul>
		</section>
</header>
