<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Tandem_Legends
 */

?>

<?php wp_footer(); ?>

		<footer>
            <div class='container'>
                <div>
                    <div>
                    <?php
                        wp_nav_menu( array(
                            'theme_location'	=> 'footer-menu',
                            'container'			=> 'ul',
                            'container_class'	=> 'footer-menu',
                            'menu_class'		=> 'footer-menu'
                        ));
                    ?>
                    </div>
                </div>
                <div>
                    <div>
                        <span class='copyright'>Tandem Legends Podcast Copyright 2021 - Not endorsed by Nintendo - The Legend of Zelda franchise is property of <a href='https://www.nintendo.com/'>Nintendo</a></span>            
                    </div>
                </div>
                <div>
                    <div class='credit'>
                        <span>Theme built by <a href='https://www.jcheiser.com'>Joe</a></span>            
                    </div>
                </div>
            </div>
        </footer>
</div><!-- #page -->

        <script src="<?php bloginfo('template_directory'); ?>/scripts/scripts.js"></script> 
</body>
</html>
